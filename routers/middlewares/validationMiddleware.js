const Joi = require('joi');

module.exports.validate = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateUser = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string(),
    username: Joi.string()
        .alphanum()
        .required(),
    createdDate: Joi.string(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateNote = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string(),
    userId: Joi.string(),
    completed: Joi.boolean(),
    text: Joi.string().required(),
    createdDate: Joi.string(),
  });

  await schema.validateAsync(req.body);
  next();
};
