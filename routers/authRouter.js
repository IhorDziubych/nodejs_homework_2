const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validate} = require('./middlewares/validationMiddleware');
const {login, register} = require('../controllers/authController');

router.post('/register', asyncWrapper(validate), asyncWrapper(register));
router.post('/login', asyncWrapper(validate), asyncWrapper(login));

module.exports = router;
