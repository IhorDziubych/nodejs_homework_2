const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validateNote} = require('./middlewares/validationMiddleware');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {getNotes, postNote, getNoteById, putNoteById,
  deleteNoteById, patchNoteById} = require('../controllers/noteController');

router.get('/', asyncWrapper(authMiddleware),
    asyncWrapper(getNotes));
router.post('/', asyncWrapper(authMiddleware),
    asyncWrapper(validateNote), asyncWrapper(postNote));
router.get('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(getNoteById));
router.put('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(validateNote), asyncWrapper(putNoteById));
router.delete('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(deleteNoteById));
router.patch('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(patchNoteById));

module.exports = router;
