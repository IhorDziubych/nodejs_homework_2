const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');

const {getUser, deleteUser,
  updateUser} = require('../controllers/userController');

router.get('/', asyncWrapper(authMiddleware), asyncWrapper(getUser));
router.delete('/', asyncWrapper(authMiddleware), asyncWrapper(deleteUser));
router.patch('/', asyncWrapper(authMiddleware), asyncWrapper(updateUser));

module.exports = router;
