const {Note} = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
  const userId = req.payload._id;
  if (!userId) {
    return res.status(400).json({message: `No user with username 
    '${req.payload.username}' found!`});
  }
  const notes = await Note.find({'userId': userId});
  if (!notes) {
    return res.status(400).json({message: `No notes found!`});
  }
  // const start = req.query.offset;
  // const end = req.query.limit;
  // const result = [];
  // for (let i = start; i <= end; i++) {
  //   if (i < notes.length) {
  //     result.push(notes[i]);
  //   }
  // }
  res.status(200).json({'notes': notes});
};

module.exports.postNote = async (req, res) => {
  const {text} = req.body;
  const userName = req.payload.username;
  if (!userName) {
    return res.status(400).json({message: `No user with username '${userName}' 
    found!`});
  }
  const note = new Note({
    userId: req.payload._id,
    text,
  });
  await note.save();
  res.status(200).json({message: 'Note created successfully!'});
};

module.exports.getNoteById = async (req, res) => {
  const noteId = req.params.id;
  const note = await Note.find({_id: noteId});
  if (!note) {
    return res.status(400).json({message: `No notes found!`});
  }
  res.status(200).json({'note': note[0]});
};

module.exports.putNoteById = async (req, res) => {
  const {text} = req.body;
  const noteId = req.params.id;
  const note = await Note.findOneAndUpdate({_id: noteId},
      {$set: {'text': text}}, {new: true});
  if (!note) {
    return res.status(400).json({message: `No notes found!`});
  }
  res.status(200).json({message: 'PUT note successfully!'});
};

module.exports.deleteNoteById = async (req, res) => {
  const noteId = req.params.id;
  const note = await Note.remove({_id: noteId});
  if (!note) {
    return res.status(400).json({message: `No notes found!`});
  }
  res.status(200).json({message: 'DELETE note successfully!'});
};

module.exports.patchNoteById = async (req, res) => {
  const noteId = req.params.id;
  const note = await Note.find({_id: noteId});
  if (!note[0]) {
    return res.status(400).json({message: `No notes found!`});
  }
  await Note.findOneAndUpdate({_id: noteId}, {$set:
    {'completed': !note[0].completed}}, {new: true});
  res.status(200).json({message: 'PATCH note successfully!'});
};
