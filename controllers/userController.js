const {User} = require('../models/userModel');
const {Credential} = require('../models/credentialModel');
const bcrypt = require('bcrypt');

module.exports.getUser = async (req, res) => {
  const userName = req.payload.username;
  const user = await User.find({'username': userName});
  if (!user) {
    return res.status(400).json({message: `No user with username 
    '${userName}' found!`});
  }
  res.status(200).json({'user': user[0]});
};

module.exports.deleteUser = async (req, res) => {
  const userName = req.payload.username;
  const user = await User.remove({'username': userName});
  await Credential.remove({'username': userName});
  if (!user) {
    return res.status(400).json({message: `No user with username 
    '${userName}' found!`});
  }
  res.status(200).json({message: 'DELETE User successfully!'});
};

module.exports.updateUser = async (req, res) => {
  const userName = req.payload.username;
  const user = await Credential.findOneAndUpdate({
    'username': userName},
  {$set: {'password': await bcrypt.hash(req.body.newPassword, 10)}},
  {new: true});
  if (!user) {
    return res.status(400).json({message: `No user with username 
    '${userName}' found!`});
  }
  res.status(200).json({message: 'UPDATE User successfully!'});
};
