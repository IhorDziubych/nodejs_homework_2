const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {Credential} = require('../models/credentialModel');
const {User} = require('../models/userModel');

module.exports.register = async (req, res) => {
  try {
    const {username, password} = req.body;
    const userCredential = new Credential({
      username,
      password: await bcrypt.hash(password, 10),
    });
    await userCredential.save();
    const user = new User({
      username,
    });
    await user.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

module.exports.login = async (req, res) => {
  try {
    const {username, password} = req.body;
    const user = await Credential.findOne({username});
    if (!user) {
      return res.status(400).json({
        message: `No user with username '${username}' 
    found!`});
    }
    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({message: `Wrong password!`});
    }
    const token = jwt.sign({username: user.username, _id: user._id},
        JWT_SECRET);
    res.status(200).json({'message': 'Success', 'jwt_token': token});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};
