const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },

  createdDate: {
    type: String,
    default: new Date().toISOString(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
