const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    default: 'Complete second homework',
  },
  createdDate: {
    type: String,
    default: new Date().toISOString(),
  },
});

module.exports.Note = mongoose.model('Note', noteSchema);
