const mongoose = require('mongoose');

const errorSchema = new mongoose.Schema({
  message: {
    type: String,
    default: Error.message,
  },
});

module.exports.User = mongoose.model('Error', errorSchema);
